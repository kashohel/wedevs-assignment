<?php

namespace App;

class Todos
{
    public $conn = '';

    public function __construct()
    {
        session_start();
        $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die('Unable to connect.');
    }

    public function lists($status = null)
    {

        if ($status == 'active') {
            $query = "SELECT * FROM todos WHERE `status` = 0;";
            $result = mysqli_query($this->conn, $query);

        } elseif ($status == 'complete') {
            $query = "SELECT * FROM todos WHERE `status` = 1;";
            $result = mysqli_query($this->conn, $query);
        } else {
            $query = "SELECT * FROM todos;";
            $result = mysqli_query($this->conn, $query);
        }

        return $result;
    }

    public function all()
    {
        $query = "SELECT * FROM todos;";
        $todos = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($todos) > 0) {
            $result['success'] = true;
            $result['data'] = $todos->fetch_all(MYSQLI_ASSOC);
            return $result;
        } else {
            $result['success'] = false;
            $result['message'] = 'No todo found';
        }
        return $result;
    }

    public function active()
    {
        $query = "SELECT * FROM todos WHERE `status` = 0;";
        $todos = mysqli_query($this->conn, $query);
        $rows = mysqli_num_rows($todos);
        if ($rows > 0) {
            $result['success'] = true;
            $result['data'] = $todos->fetch_all(MYSQLI_ASSOC);
            $result['count'] = $rows." item left";
            return $result;
        } else {
            $result['success'] = false;
            $result['message'] = 'No todo found';
            $result['count'] = 'No todo found';
        }
        return $result;
    }

    public function complete()
    {
        $query = "SELECT * FROM todos WHERE `status` = 1;";
        $todos = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($todos) > 0) {
            $result['success'] = true;
            $result['data'] = $todos->fetch_all(MYSQLI_ASSOC);
            return $result;
        } else {
            $result['success'] = false;
            $result['message'] = 'No complete todo found';
        }
        return $result;
    }


    public function store($name)
    {
        if ($name) {
            $query = "INSERT INTO `todos` (`name`, `status`) "
                . " VALUES ('" . $name . "', 0)";

            if (mysqli_query($this->conn, $query)) {
                $_SESSION['Massage'] = "Yes Successfully submited";
            } else {
                $_SESSION['Message'] = "Not submited";
            }
            header('location:index.php');

        } else {
            $_SESSION['Message'] = "Not submitted";
            header('location:index.php');
        }
    }

    public function clear_complete()
    {
        $query = "DELETE FROM `todos` WHERE `status` = 1";
        if (mysqli_query($this->conn, $query)) {
            return "Success";
        } else {
            return "Can not delete";
        }
    }

    public function change_status($id = null)
    {

        if ($id) {
            $query = "SELECT status FROM `todos` WHERE `id` = " . $id . ";";

            $rows = mysqli_query($this->conn, $query);
            $row = mysqli_fetch_row($rows);
            if ($row) {
                if ($row[0]) {
                    $u_query = "UPDATE `todos` SET `status` = '0' WHERE `id` = " . $id . ";";
                } else {
                    $u_query = "UPDATE `todos` SET `status` = '1' WHERE `id` = " . $id . ";";
                }
                if (mysqli_query($this->conn, $u_query)) {
                    return "Status update successful";
                } else {
                    return "Status can not update";
                }
            } else {
                return "Todo not found.";
            }
        } else {
            return "Id not found";
        }
    }

    public function delete($id = null)
    {
        if ($id) {
            $query = "SELECT status FROM `todos` WHERE `id` = " . $id . ";";

            $rows = mysqli_query($this->conn, $query);
            $row = mysqli_fetch_row($rows);
            if ($row) {
                $d_query = "DELETE FROM `todos` WHERE `id` = " . $id . ";";
                if (mysqli_query($this->conn, $d_query)) {
                    return "Todo delete successful";
                } else {
                    return "Todo can not update";
                }
            } else {
                return "Todo not found.";
            }
        } else {
            return "Id not found";
        }
    }

    public function rename($id, $name)
    {
        if ($id) {
            $query = "SELECT name FROM `todos` WHERE `id` = " . $id . ";";

            $rows = mysqli_query($this->conn, $query);
            $row = mysqli_fetch_row($rows);
            if ($row) {
                $u_query = "UPDATE `todos` SET `name` = '" . $name . "' WHERE `id` = " . $id . ";";

                if (mysqli_query($this->conn, $u_query)) {
                    return "Todo rename successful";
                } else {
                    return "Todo can not rename";
                }
            } else {
                return "Todo not found.";
            }
        } else {
            return "Id not found";
        }
    }

    public function __destruct()
    {
        mysqli_close($this->conn);
    }

}
