$(document).ready(function () {
    loadData();
});

function loadData() {
    $.ajax({
        url: "actions/GetTodos.php",
        type: "post",
        data: {status: "all"},
        success: function (response) {
            var jsonData = JSON.parse(response);
            var table = $("#all_table");
            var row = "";
            if (jsonData.success) {
                table.html('');
                $.each(jsonData.data, function (key, rowData) {
                    if (rowData.status == 1) {
                        row = '<tr><td><input type="checkbox" checked="checked" onchange="status(' + rowData.id + ')"></td>\n' +
                            '<td><del>' + rowData.name + '</del></td><td><button class="delete" onclick="deletion(' + rowData.id + ')">x</button></td></tr>';
                    } else {
                        row = '<tr><td><input type="checkbox" onchange="status(' + rowData.id + ')"></td>\n' +
                            '<td><input type="text" class="todo_name" value="'+rowData.name+'" onchange="rename('+rowData.id+', this.value)"></td><td><button class="delete" onclick="deletion(' + rowData.id + ')">x</button></td></tr>';
                    }
                    table.append(row);
                });
            } else {
                table.html("<tr><td style='text-align:center'>" + jsonData.message + "</td></tr>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    $.ajax({
        url: "actions/GetTodos.php",
        type: "post",
        data: {status: "active"},
        success: function (response) {
            var jsonData = JSON.parse(response);
            var table = $("#active_table");
            var row = "";
            if (jsonData.success) {
                table.html('');
                $.each(jsonData.data, function (key, rowData) {
                    row = '<tr><td><input type="checkbox" onchange="status(' + rowData.id + ')"></td>\n' +
                        '<td><input type="text" class="todo_name" value="'+rowData.name+'" onchange="rename('+rowData.id+', this.value)"></td><td><button class="delete" onclick="deletion(' + rowData.id + ')">x</button></td></tr>';
                    table.append(row);
                });
            } else {
                table.html("<tr><td style='text-align:center'>" + jsonData.message + "</td></tr>");
            }
            $("#counter").html(jsonData.count);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    $.ajax({
        url: "actions/GetTodos.php",
        type: "post",
        data: {status: "complete"},
        success: function (response) {
            var jsonData = JSON.parse(response);
            var table = $("#complete_table");
            var row = "";
            if (jsonData.success) {
                table.html('');
                $.each(jsonData.data, function (key, rowData) {
                    row = '<tr><td><input type="checkbox" checked="checked" onchange="status(' + rowData.id + ')"></td>\n' +
                        '<td><del>' + rowData.name + '</del></td><td><button class="delete" onclick="deletion(' + rowData.id + ')">x</button></td></tr>';
                    table.append(row);
                });
            } else {
                table.html("<tr><td style='text-align:center'>" + jsonData.message + "</td></tr>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}


//Submit form data from index file to Store.php file

$(".entry-form").submit(function () {
    event.preventDefault();
    var name = $('input[name="new_todo"]').val();
    $('input[name="new_todo"]').val("");
    $.ajax({
        url: "actions/Store.php",
        type: "post",
        data: {name: name},
        success: function (response) {
            name.val("");
            // You will get response from your PHP page (what you echo or print)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    loadData();
});

//Clear all complete todos

function clearComplete() {
    $.ajax({
        url: "actions/ClearComplete.php",
        type: "post",
        data: "clear",
        success: function (response) {
            // You will get response from your PHP page (what you echo or print)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    loadData();
}

//Change todos status

function status(id) {
    $.ajax({
        url: "actions/ChangeStatus.php",
        type: "post",
        data: {id: id},
        success: function (response) {
            // You will get response from your PHP page (what you echo or print)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    loadData();
}

//Deletion a single t odo

function deletion(id) {
    $.ajax({
        url: "actions/Delete.php",
        type: "post",
        data: {id: id},
        success: function (response) {
            // You will get response from your PHP page (what you echo or print)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    loadData();
}

//Rename a single t odo
function rename(id, name) {
    $.ajax({
        url: "actions/Rename.php",
        type: "post",
        data: {id: id, name: name},
        success: function (response) {
            // You will get response from your PHP page (what you echo or print)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
    loadData();
}
