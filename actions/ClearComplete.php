<?php
include_once '../vendor/autoload.php';

use App\Todos;

if (isset($_POST)) {

    $todo = new Todos();
    echo $todo->clear_complete();

} else {

    echo "You can not access this page.";

}
?>