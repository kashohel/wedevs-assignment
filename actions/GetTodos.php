<?php
include_once '../vendor/autoload.php';

use App\Todos;

$todo = new Todos();

if (isset($_POST['status']) && !null == $_POST['status']) {
    if ($_POST['status'] == 'complete'){
        $result = $todo->complete();
    } elseif ($_POST['status'] == 'active'){
        $result = $todo->active();
    } elseif($_POST['status'] == 'all') {
        $result = $todo->all();
    }

    echo json_encode($result);

} else {
    echo "You can not access this page.";
}
?>