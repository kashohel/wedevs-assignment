<?php
include_once '../vendor/autoload.php';

use App\Todos;

$todo = new Todos();

if (isset($_POST['id']) && !null == $_POST['id']) {

    echo $todo->delete($_POST['id']);

} else {

    echo "You can not access this page.";

}
?>