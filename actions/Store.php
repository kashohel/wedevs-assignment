<?php
include_once '../vendor/autoload.php';

use App\Todos;

$todo = new Todos();

if (isset($_POST['name']) && !null == $_POST['name']) {

    echo $todo->store($_POST['name']);

} else {
    echo "You can not access this page.";

}
?>