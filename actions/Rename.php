<?php
include_once '../vendor/autoload.php';

use App\Todos;

$todo = new Todos();

if (isset($_POST['id']) && !null == $_POST['name']) {

    echo $todo->rename($_POST['id'], $_POST['name']);

} else {
    echo "You can not access this page.";

}
?>