<?php
include_once 'vendor/autoload.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Neon Admin Panel"/>
    <meta name="author" content=""/>

    <title><?= SITE_TITLE ?></title>
    <script type="text/javascript" charset="UTF-8" src="assets/jquery-3.4.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="assets/style.css">
</head>

<body>
<div class="saction">
    <div class="entry">
        <label>Todos</label>
        <form method="post" action="actions/Store.php" class="entry-form">
            <input type="text" name="new_todo" placeholder="What needs to be done?">
        </form>
    </div>
    <div class="result">

        <!-- Tab content fot all -->
        <div id="all" class="tabcontent" style="display: block;">
            <table id="all_table">
<!--               All todos data will be load here-->
            </table>
        </div>

        <!-- Tab content fot active -->
        <div id="active" class="tabcontent">
            <table id="active_table">
                <!--   Active todos data will be load here-->
            </table>
        </div>

        <!-- Tab content fot complete -->
        <div id="complete" class="tabcontent">
            <table id="complete_table">
<!--               Complete todos data will be load here-->
            </table>
        </div>

        <div class="tab">
            <button id="counter"></button>
            <button class="tablinks active" onclick="openTodo(event, 'all')">All</button>
            <button class="tablinks" onclick="openTodo(event, 'active')">Active</button>
            <button class="tablinks" id="complete_button" onclick="openTodo(event, 'complete')">Completed</button>
            <button id="clear_button" onclick="clearComplete()">Clear Completed</button>
        </div>


    </div>
</div>
<script type="text/javascript" charset="UTF-8" src="assets/custom.js"></script>
<script type="text/javascript" charset="UTF-8" src="assets/ajax.js"></script>
</body>
</html>