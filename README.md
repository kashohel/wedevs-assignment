# WeDevs Assignment

This todo manager can do task to create new todo, modify name, modify status and deletion.


**Requirement:**

1) PHP 5.6 or higher
2) MySql database

**Installation:**
1) Put file inside any accessible server directory.
2) Create a new database and import _wedevs.sql_ file.
3) Edit config.php file and provide database connection info.

Now run this system by hit on index.php file or it's root directory.

This system is develop by [Khorshed Alam](http://alam.foxof.org).


 